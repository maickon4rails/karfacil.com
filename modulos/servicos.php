<div align="center">
	<h1>Nossos servi�os</h1>
	<p>Nesta p�gina vamos explicar como que funciona os nossos servi��s de cadastramento. 
	Atrav�s de uma liga��o voc� estar� investindo na divulga��o de seu estabelecimento.</p>
	<h1>As lojas de ve�culos</h1>
	<p>Uma vez feita a liga��o, estaremos combinando o dia e a hora em que visetaremos o seu estabelecimento
	para o cadastramento de seus ve�culos em nosso sistema. Este processo consiste em 3 passos:</p>
	<p>
	<ul>
		<li>Passo 1-Cadastramento do dono da loja</li>
		<li>Passo 2-Cadastramento da loja</li>
		<li>Passo 3-Cadastramento dos ve�culos</li>
	</ul>
	<p>
	No passo 1, precisamos desse cadastro para que o sistema saiba a quem a loja perten�e, 
	al�m disso fica mais f�cil de encontrar os dados cadastrais do dono pelo fato de estar bem organizado no sistema. 
	</p>
	<p>No passo 2, estaremos cadastrando a loja pois a loja ter� uma p�gina de apresenta��o e nesta p�gina,
	o cliente encontrar� a logomarca da loja, logo abaixo uma descri��o do endere�o de onde o estabelecimento se 
	encontra. E mais abaixo ainda uma foto do estabelecimento que ser� tirada no dia do cadastramento dos ve�culos. </p>
		
	<p>No passo 3, estaremos cadastrandos os ve�culos na devida loja cadastrada em nosso sistema.</p>
	
	<p>Ambas as imagens apresentadas nesta p�gina, s�o links que quando clicados levar�o o cliente para p�gina
	de visualiza��o dos ve�culos da loja.</p>
	
	<p>A p�gina de visualiza��o de ve�culo, consiste num slide que apresenta intercaladamente cada uma das imagens
	do ve�culo cadastrado. Abaixo se encontra a descri��o do ve�culo que � fornecida pela loja respons�vel, 
	o endere�o e dados para contato. Mais abaixo ainda em vermelho, um trecho visando ao cliente que diz que 
	todas as informa��es presentes ali s�o de inteira responsabilidade da loja respons�vel.</p>
	
	<h1>As propagandas</h1>
	<p>O cadastro de propagandas � bem mais simples do que o cadastro de lojas de ve�culos, pois seu cadastro
	compreende em 2 passos.</p>
	<ul>
		<li>Passo 1-Cadastramento do dono de propaganda</li>
		<li>Passo 2-Cadastramento da propaganda em si</li>
	</ul>
	
	<p>No passo 1, vamos cadastrar os dados do dono do estabelecimento. Este passo � muito parecido com o cadastro de 
	dono de loja de ve�culos.</p>
	<p>No passo 2 cadastraremos a propaganda. A propaganda cadastrada, ter� direito a 4 imagens que ser�o apresentadas
	dentro de um slide. Neste slide as imagens ficar�o se intercalando, permitindo que o cliente visualize todos os 
	4 slides.</p>
	<p>Mais abixo o endere�o do estabelecimento e uma descri��o que pode abordar o assunto que o dono desejar.</p>
	<p>
	<h1>Ve�culos exclusivos</h1>
	<p>Nesta parte tratamos de clientes que desejam se desfazer de seus ve�culos por si s�. Ent�o para facilitar sua vida,
	cadastramos o seu ve�culo em nosso sistema e colocamos os dados do dono para contato. Dessa forma ele poder� encontrar
	um comprador por interm�dio da karfacil.com. Apenas 2 passos s�o necess�rios para que esse cadastro seja possivel.</p>
	<ul>
		<li>Passo 1-Cadastramento do dono do ve�culo</li>
		<li>Passo 2-Cadastramento do ve�culo em si</li>
	</ul>
	<p>No passo 1, vamos cadastrar o nosso cliente no sistema.</p>
	<p>No passo 2, quando ja temos um cliente cadastrado, cadastramos o ve�culo desejado associado ao seu respectivo dono.</p>
	<p>A p�gina de ve�culos exclusivos consiste na apresenta��o dos dados para contato do cliente cadastrado e logo
	abaixo dentro de um slide, temos as 4 imagens de que o cliente tem direito para a visualiza��o do ve�culo.
	As imagens passam intercaladamente permitindo que seja visto todas as imagens.</p>
</div>	